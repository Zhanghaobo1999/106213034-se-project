<?php
require("utils.php");
chkAccess(9,"main.php");
require("prdModel.php");


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Basic HTML Examples</title>
</head>
<body>
<p>This is Product MAIN page 
[<a href="logout.php">logout</a>]
[<a href = "admin.php">Go to Admin page</a>]
</p>
<hr>
<?php
	echo "Hello ", $_SESSION["loginProfile"]["uName"],
	", Your ID is: ", $_SESSION["loginProfile"]["uID"],
	", Your Role is: ", $_SESSION["loginProfile"]["uRole"],"<hr>";
?>


<table width="300" border="1">
  <tr>
    <td >id</td>
    <td align = center>name</td>
    <td align = center>price</td>
    <td align = center>+</td>
	<td align = center>-</td>
  </tr>
<?php
$result=getPrdList();
while (	$rs=mysqli_fetch_assoc($result)) {
	echo "<tr><td>" . $rs['prdID'] . "</td>";
	echo "<td align = center>{$rs['name']}</td>";
	echo "<td align = right>" , $rs['price'], "</td>";
	echo "<td align = center><a href='prd.editUI.php?id=" , $rs['prdID'] , "'>Edit</a></td>";
	echo "<td align = center><a href='prd.del.php?id=" , $rs['prdID'] , "' onclick='return confirm(\"Sure?\")'>Del</a></td></tr>";
}
?>
</table>
<a href="prd.editUI.php?id=-1">Add New Product</a>

</body>
</html>
